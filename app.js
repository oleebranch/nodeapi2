const express = require('express')

const fs = require('fs')

const app = express()

app.use(express.static(__dirname))

app.get('/', (req, res) => {
  let data = fs.readFileSync('./data/alien.json')
  data = JSON.parse([data])
  res.send(data)
})

const port = 5000
app.listen(port, () => {
  console.log(`The app is running @ http://localhost:${port}`)
})
